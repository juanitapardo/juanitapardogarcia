#include <CapacitiveSensor.h>

// 10 megohm resistor between pins 4 & 2, pin 2 is sensor pin, add wire, foil
CapacitiveSensor touchSwitch = CapacitiveSensor(4, 2);

const int buzzer = 9; //buzzer to arduino pin 9

void setup()
{
 pinMode(buzzer, OUTPUT);
 Serial.begin(9600);
}

void loop()
{
 long start = millis();
 long val = touchSwitch.capacitiveSensor(100);

tone(buzzer, val); // Send 1KHz sound signal...
  
  
 // print sensor output 1
 Serial.println(val);
}
